# PLP

This example loads products from a non common API
and it category ID is loaded from an outher application 
like PHP for example.

The outher application injects the category ID into REACT and the 
react application call the api using axios with the category ID

AS same it use filters that are updated from the API aswell by demand.

## Webpack
this react example does not use `create-react-app`, was created from scratch with webpack

###Install

``cd app``

``npm install``

``cd react``

### Running dev

``npm run watch``

### Running Production

``npm run build``