<?php
$user = (object)[
    'name' => 'Jane Doe',
    'email' => 'janedoe@gmail.com',
    'logged' => true
];

$assets = (object)[
    'css' => ($_SERVER['SERVER_NAME'] == 'plp.test') ? '/app/assets/css/app.css' : '/react/app/assets/css/app.css',
    'js' => ($_SERVER['SERVER_NAME'] == 'plp.test') ? '/app/assets/bundle/main.bundle.js' : '/react/app/assets/bundle/main.bundle.js',
    'url' => 'https://plp.test/'
];

if (isset($_GET['keywords'])) {
    $categoryId = "'search'";
    $keywords = "'" . $_GET['keywords'] . "'";
} else {
    if (isset($_GET['category'])) {
        $categoryId = (int)$_GET['category'];
        $keywords = "null";
    }else{
        $categoryId = 15;
        $keywords = "null";
    }
}

$textRandom = [
        [
            'title' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in aliquam ante, id luctus 
            augue. Cras semper euismod scelerisque. Quisque porttitor metus quis lobortis imperdiet. Donec accumsan 
            scelerisque quam at tincidunt. Sed aliquam nibh ac dolor porttitor vehicula.',
        ],
        [
            'title' => 'Duis in aliquam ante, id luctus augue',
            'description' => 'Pellentesque consectetur, nisl a tristique porttitor, erat sem interdum ante, vitae cursus 
            nibh ex at ex. Suspendisse efficitur felis sit amet lacus molestie congue. Aliquam pellentesque ut turpis 
            vel elementum.',
        ],
        [
            'title' => 'Duis consectetur nibh quis placerat pellentesque',
            'description' => 'Vestibulum malesuada tempus eros nec luctus. Sed enim enim, mollis id dapibus vel, 
            sagittis id magna. Sed gravida euismod tempus. Vestibulum non est nisl. Nulla nisi dolor, ornare a 
            elementum eget, fermentum nec odio. Vestibulum accumsan luctus orci sed ultricies. Suspendisse 
            dignissim suscipit varius. Vestibulum scelerisque id justo vitae lobortis.',
        ]
];
$rand = rand(0, count($textRandom)-1);

?>
<!doctype html>
<html lang="en">
<head>
    <title>React PLP</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= $assets->css ?>?cache=<?= date('YmdHis') ?>" type="text/css">
</head>
<script type="text/javascript">
    var STATIC_URL = '<?=$assets->url?>';
    var outerApplication = {
        categoryId: <?=$categoryId?>,
        keywords: <?=$keywords?>,
        baseApiUrl: 'https://www.myURL.com',
        shareLink: '<?= (isset($_GET['cr']))? $_GET['cr'] : ''; ?>',
    };
</script>
<body>

<div class="container">
    <div class="top_banner" style="position:relative;margin-bottom:20px;">
        <h1 style="display:block;font-size:28px;margin:0px 0px 10px;text-align:center;font-weight:bold;">
            <?=$textRandom[$rand]['title']?>
        </h1>
        <p style="display:block;margin:10px;overflow:hidden;text-align:center;font-size:14px;">
            <?=$textRandom[$rand]['description']?>
        </p>
    </div>
</div>
<div>
    <div id="app"></div>
</div>

<script type="text/javascript" src="<?= $assets->js ?>?cache=<?= date('YmdHis') ?>"></script>

</body>
</html>
