import company from '../api/company';

const fetchReviews = (productIds, baseApiUrl) => {
    let formData = new FormData();
    formData.append('products', JSON.stringify(productIds));

    return company(baseApiUrl, true)
        .post('/api.trustpilot_reviews.php', formData, {headers: {'Content-Type': 'multipart/form-data'}})
        .then(response => {
            return response.data;
        });
};

export default fetchReviews;