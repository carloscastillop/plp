import company from '../api/company';

const fetchFilters = (selectedFilters, apiUrl, shareLinkCr = null) => {
    let formData = new FormData();
    let shareLinkParam = '';
    selectedFilters.forEach(filter => {
        if (filter.hasOwnProperty('categoryId')) {
            formData.append('categoryId', filter.categoryId);
            if (filter.hasOwnProperty('keywords')) {
                formData.append('keywords', filter.keywords);
            }
        } else {
            formData.append(filter.id, filter.value);
        }
    });
    if (shareLinkCr) {
        shareLinkParam = '?cr=' + encodeURI(shareLinkCr);
    }

    return company(apiUrl, true)
        .post('/ajax.cat_refine_json.php' + shareLinkParam,
            formData,
            {
                headers: {'Content-Type': 'multipart/form-data'}
            }
        )
        .then(response => {
            return response.data.data;
        });
};

export default fetchFilters;