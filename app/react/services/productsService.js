import company from '../api/company';

const fetchProducts = (selectedFilters, offset = 0, sortBy = null, baseApiUrl, shareLinkCr = null) => {
    let formData = new FormData();
    let shareLinkParam = '';
    selectedFilters.forEach(filter => {
        if (filter.hasOwnProperty('categoryId')) {
            formData.append('categoryId', filter.categoryId);
            if (filter.hasOwnProperty('keywords')) {
                formData.append('keywords', filter.keywords);
            }
        } else {
            if(filter.type === 'checkbox') {
                formData.append(filter.id, 1);
            } else {
                formData.append(filter.name, filter.value);
            }
        }
        if(shareLinkCr){
            shareLinkParam = '?cr=' + encodeURI(shareLinkCr);
        }
    });

    formData.append('offset', offset);

    if (sortBy) {
        formData.append('ns', sortBy);
    }

    return company(baseApiUrl, true)
        .post('/ajax.cat_refine_products_json.php' + shareLinkParam, formData, {headers: {'Content-Type': 'multipart/form-data'}})
        .then(response => {
            return response.data;
        });
};

export default fetchProducts;