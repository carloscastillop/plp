let values = {
    isDebug: true,
    imageURL: '',
    productionUrls: false,
    GAtrackingId: 'UA-317176-3',
    apiUrl: 'https://www.myURLDEV.com'
};

//set the environment
const environmentSwitch = process.env.REACT_APP_MODE || process.env.NODE_ENV;

switch(environmentSwitch){
    case 'production':
        values.productionUrls = true;
        values.isDebug = false;
        values.apiUrl = 'https://www.myURL.com';
        break;
    default:
        break;
}

export default values;