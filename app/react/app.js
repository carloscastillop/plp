import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';
import ProductsAndFilters from './components/ProductsAndFilters/ProductsAndFilters';
import ErrorBoundary from './components/ErrorBoundary/ErrorBoundary';
import 'react-app-polyfill/ie11';

/** We are importing our index.php outerApplication Variable */
import outerApplication from 'outerApplication';

/* globals __webpack_public_path__ */
__webpack_public_path__ = `${window.STATIC_URL}/app/assets/bundle/`;

class Myapp extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { categoryId, keywords, baseApiUrl, shareLink } = outerApplication;

        return (
            <ErrorBoundary>
                <Fragment>
                    <ProductsAndFilters
                        categoryId={categoryId}
                        keywords={keywords}
                        baseApiUrl={baseApiUrl}
                        shareLink={shareLink}
                    />
                </Fragment>
            </ErrorBoundary>
        )
    }
}

render(<Myapp/>, document.getElementById('app'));