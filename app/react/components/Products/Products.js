import React from 'react';
import debounce from 'lodash.debounce';

import ProductBlock from '../ProductBlock/ProductBlock';
import fetchProducts from '../../services/productsService';
import fetchReviews from '../../services/reviewsService';

import grid from '../../modules/Grid.Module.scss';
import cards from "../../modules/Cards.module.scss";
import spacing from "../../modules/Spacing.module.scss";
import forms from "../../modules/Forms.module.scss";
import utilities from "../../modules/Utilities.modules.scss";
import buttons from '../../modules/Buttons.module.scss';
import BannerBlock from '../BannerBlock/BannerBlock';
import ShareLink from '../ShareLink/ShareLink';

class RenderProducts extends React.Component {
    constructor(props) {
        super(props);

        const {selectedFilters, enableInfiniteScroll, onclickGAEvent, baseApiUrl, shareLink} = props;
        this.state = {
            isLoading: false,
            loadFromScrolling: false,
            hasMore: true,
            products: {
                offset: -1,
                data: [],
                count: 0,
                share_link: null
            },
            selectedFilters: selectedFilters,
            reviews: [],
            sortBy: '',
            productCounting: 0,
            baseApiUrl: baseApiUrl,
            shareLink: shareLink,
            firstSortBy: ''
        };
        if (enableInfiniteScroll) {
            window.onscroll = debounce(() => {
                const {
                    loadProducts,
                    state: {
                        isLoading,
                        hasMore,
                        selectedFilters,
                        sortBy
                    }
                } = this;
                const gap = 1000; /*a gap to trigger the loading more...*/
                if (isLoading || !hasMore) {
                    return;
                }

                // if ((window.innerHeight + document.documentElement.scrollTop) === (document.documentElement.offsetHeight)) {
                // if ((document.documentElement.scrollTop) === (document.documentElement.offsetHeight - window.innerHeight - gap)) {
                if ((document.documentElement.scrollTop + window.innerHeight + gap) > (document.getElementById("bottomProducts").offsetTop)) {
                    loadProducts(selectedFilters, true, sortBy, true);
                }

            }, 100);
        }
    }

    componentDidMount() {
        const {selectedFilters, shareLink} = this.state;
        this.loadProducts(selectedFilters, true, '', false, shareLink);
    }

    componentDidUpdate(prevProps) {
        if (this.props.selectedFilters !== prevProps.selectedFilters) {
            this.loadProducts(this.props.selectedFilters, false);
        }
    }

    loadProducts = (selectedFilters, append = true, sortBy = '', loadFromScrolling = false, shareLink = null) => {
        if (loadFromScrolling) {
            this.setState({loadFromScrolling: true});
        } else {
            this.setState({loadFromScrolling: false, isLoading: true});
        }

        const {products, reviews, baseApiUrl} = this.state;

        fetchProducts(selectedFilters, append ? products.offset + 1 : 0, sortBy, baseApiUrl, shareLink).then(nextProducts => {
            let combinedProductData = [];
            let orderByDefault = 'DEFAULT';
            if (append) {
                combinedProductData = [
                    ...products.data,
                    ...nextProducts.data
                ];
            } else {
                combinedProductData = [...nextProducts.data];
            }

            if(nextProducts.orderBy !== ''){
                orderByDefault = nextProducts.orderBy
                this.selectElement('ns_check', orderByDefault );
            }

            this.setState({
                products: {...nextProducts, data: combinedProductData},
                isLoading: false,
                loadFromScrolling: false,
                hasMore: combinedProductData.length < nextProducts.count,
                selectedFilters: selectedFilters,
                sortBy: (nextProducts.orderBy !== '') ? nextProducts.orderBy : 'DEFAULT'
            });


            const reviewProductIds = [
                ...nextProducts.data.map(product => product.id)
            ];
            fetchReviews(reviewProductIds, baseApiUrl).then(nextReviews => {
                let combinedReviews = [
                    ...reviews,
                    ...nextReviews
                ];

                this.setState({
                    reviews: combinedReviews
                });
            });
        });
    };

    loadMoreProductsBtn = debounce(() => {
        const {
            loadProducts,
            state: {
                isLoading,
                hasMore,
                selectedFilters,
                sortBy
            }
        } = this;
        if (isLoading || !hasMore) {
            return;
        }

        loadProducts(selectedFilters, true, sortBy, true);

    }, 100);

    handleSort = sortBy => {
        const {selectedFilters} = this.state;
        this.loadProducts(selectedFilters, false, sortBy);
    };

    calculateLoadedProducts() {
        const {products} = this.state;
        const {count, offset, limit, pages, share_link} = products;
        let totalCount = 0;
        totalCount = (offset + 1) * limit;
        if ((offset + 1) == pages) {
            totalCount = totalCount - (totalCount - count);
        }
        return totalCount;
    }

    selectElement(id, valueToSelect) {
        let element = document.getElementById(id);
        element.value = valueToSelect;
    }

    render() {
        const {enableInfiniteScroll, onclickGAEvent} = this.props;
        const {products, isLoading, loadFromScrolling, reviews} = this.state;

        if (products.length == 0) return '';

        const {data, count, pages, offset, share_link, preezie} = products;

        return (
            <div>
                <div className={cards.card + ' ' + spacing.mb4}>
                    <div className={cards.cardBody}>
                        <div className={forms.formInline + ' ' + utilities.floatLeft}>
                            <div className={forms.formGroup + ' ' + spacing.mb0}>
                                <label htmlFor="ns_check">Sort by: </label>
                                <select
                                    name="ns_check"
                                    id="ns_check"
                                    className={forms.formControl}
                                    onChange={e => {
                                        this.handleSort(e.target.value);
                                        let index = e.nativeEvent.target.selectedIndex;
                                        onclickGAEvent({
                                            action: 'FP - Sort By',
                                            label: e.target[index].text,
                                        })

                                    }}
                                    defaultValue={'DEFAULT'}
                                >
                                    <option value="DEFAULT">Recommended</option>
                                    <option value="bestsell">Top Sellers</option>
                                    <option value="price">Price Low to High</option>
                                    <option value="pricehl">Price High to Low</option>
                                    <option value="name">Alphabetical: A to Z</option>
                                    <option value="review">Top Rated</option>
                                </select>
                            </div>
                        </div>
                        {
                            (count > 1) &&
                            <span className={utilities.floatRight + ' ' + spacing.mt1 + ' ' + spacing.pt1}>
                                <strong>{count}</strong> product{(count > 1) ? 's' : ''} found
                            </span>
                        }
                    </div>
                </div>
                {
                    isLoading &&
                    <div className={utilities.loaderContainer}>
                        <div className={utilities.loader}></div>
                        <span>Loading...</span>
                    </div>
                }
                {
                    !isLoading &&
                    <div className={grid.flexGrid}>
                        {
                            data.map((item, index) => {
                                return <React.Fragment
                                    key={'product-block-banner-' + index}
                                >
                                    {index === 8 &&
                                        <BannerBlock preezie={preezie}/>
                                    }
                                    <ProductBlock
                                        side={((index + 1) % 4) == 0 ? 'Left' : 'Right'}
                                        key={'product-block-' + item.id}
                                        featured={index < 4}
                                        item={item}
                                        review={reviews ? reviews.find(review => review.productId === item.id) : null}
                                        onclickGAEvent={onclickGAEvent}
                                    />
                                </React.Fragment>
                            })
                        }
                    </div>
                }
                {
                    loadFromScrolling &&
                    <div className={utilities.loaderContainer}>
                        <div className={utilities.loader}></div>
                        <span>Loading more products...</span>
                    </div>
                }
                {
                    (!isLoading && !enableInfiniteScroll && !loadFromScrolling) &&
                    <div className={utilities.textCenter + ' ' + spacing.my4}>
                        <span className={utilities.dBlock + ' ' + utilities.textMuted + ' ' + spacing.mb4}>
                            Currently displaying 1-{this.calculateLoadedProducts()} of {count} products
                        </span>
                        {
                            (pages !== (offset + 1)) &&
                            <button
                                className={buttons.btn + ' ' + buttons.btnPrimary + ' ' + buttons.btnLg}
                                onClick={this.loadMoreProductsBtn}
                            >
                                View more products
                            </button>
                        }
                    </div>
                }
                <div id='bottomProducts'></div>
                {
                    share_link &&
                    <ShareLink link={share_link}/>
                }
            </div>
        );
    }
}

export default RenderProducts;