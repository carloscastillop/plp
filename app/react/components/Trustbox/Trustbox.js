import React, {Component} from 'react';

import style from "../../modules/Trustbox.module.scss";

class TrustBox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            hover: false
        };
        this.reviewRef = React.createRef();
        this.breakdownRef = React.createRef();
        this.timer = 0;
    }

    handleMouseOutside = (event) => {

        if (
            this.breakdownRef.current && !this.breakdownRef.current.contains(event.target)
            && this.reviewRef.current && !this.reviewRef.current.contains(event.target)
        ) {
            if (this.timer) {
                clearTimeout(this.timer)
            }

            document.removeEventListener('mousemove', this.handleMouseOutside);

            this.setState({
                hover: false
            })
        }
    };

    hover = () => {
        this.timer = setTimeout(this.hoverDelayed, 500);
    }

    hoverDelayed = () => {
        if (this.timer) {
            clearTimeout(this.timer)
        }
        document.addEventListener('mousemove', this.handleMouseOutside);

        this.setState({
            hover: true
        });

    }

    renderStars(rating) {

        return [1, 2, 3, 4, 5].map((key) => {

            let colour = '#00b77b';
            if (rating < 5) colour = '#71cf15';
            if (rating < 4) colour = '#fece00';
            if (rating < 3) colour = '#ff8521';
            if (rating < 2) colour = '#fe3721';
            if (rating < 1) colour = '#e5e5e5';

            if (key > rating) colour = '#e5e5e5';

            return (
                <g
                    key={key}
                    transform={`translate(${(key - 1) * 50}, 0)`}>
                    <path
                        className="tp-star__canvas"
                        fill={colour}
                        d="M0 46.330002h46.375586V0H0z"/>
                    <path className="tp-star__shape"
                          d="M39.533936 19.711433L13.230239 38.80065l3.838216-11.797827L7.02115 19.711433h12.418975l3.837417-11.798624 3.837418 11.798624h12.418975zM23.2785 31.510075l7.183595-1.509576 2.862114 8.800152L23.2785 31.510075z"
                          fill="#FFF"/>
                </g>
            )
        });
    }

    renderHover() {
        const {review} = this.props;
        const {hover} = this.state;

        if (!hover) return;
        if (!review || !review.average) return;

        const reviewBreakdown = {
            'total': (review && review.numberOfReviews && review.numberOfReviews.total),
            5: (review && review.numberOfReviews && review.numberOfReviews.fiveStars),
            4: (review && review.numberOfReviews && review.numberOfReviews.fourStars),
            3: (review && review.numberOfReviews && review.numberOfReviews.threeStars),
            2: (review && review.numberOfReviews && review.numberOfReviews.twoStars),
            1: (review && review.numberOfReviews && review.numberOfReviews.oneStar),
        };

        const breakdown = [5, 4, 3, 2, 1].map((key) => {

            //set bar colour
            let colour = '#00b77b';
            if (key === 4) colour = '#71cf15';
            if (key === 3) colour = '#fece00';
            if (key === 2) colour = '#ff8521';
            if (key === 1) colour = '#fe3721';

            return (
                <div
                    key={key}
                    className={style.row}>
                    <div
                        className={style.key}>
                        {`${key} star${key === 1 ? '' : 's'}`}
                    </div>
                    <div
                        className={style.bar}>
                        <div
                            style={{
                                background: colour,
                                width: `${Math.round((reviewBreakdown[key] / reviewBreakdown['total']) * 100)}%`
                            }}/>
                    </div>
                    <div
                        className={style.count}>
                        ({reviewBreakdown[key] || 0})
                    </div>
                </div>
            )
        });

        return (
            <div
                ref={this.breakdownRef}
                className={style.breakdown}>
                <div className={style.inner}>
                    <div
                        className={style.rating}>
                        {review && review.average} out of 5
                    </div>
                    <div>
                        {breakdown}
                    </div>
                </div>
            </div>
        )
    }

    render() {
        const {review, numReviews} = this.props;

        return (
            <div
                ref={this.reviewRef}
                className={style.trustpilotReviewBlock}
                style={{
                    cursor: (review && review.average) ? 'pointer' : 'default'
                }}
                onMouseEnter={() => this.hover(true)}>


                <svg
                    viewBox="0 0 251 46"
                    xmlns="http://www.w3.org/2000/svg"
                    style={{
                        height: 15,
                        marginRight: 2,
                        verticalAlign: 'text-bottom'
                    }}>
                    {this.renderStars(Math.round((review && review.average) || 0))}
                </svg>

                {numReviews}

                <svg
                    viewBox="21.03125 32.75005340576172 57.9376220703125 34.5"
                    width={10}
                    height={10}
                    xmlns="http://www.w3.org/2000/svg"
                    style={{
                        marginLeft: '2px'
                    }}>
                    <g transform="matrix(0, 1, -1, 0, 1052.3623046875, 0.00005900000542169437)">
                        <path
                            style={{
                                textIndent: 0,
                                textTransform: 'none',
                                direction: 'ltr',
                                blockProgression: 'tb',
                                baselineShift: 'baseline',
                                color: '#000000',
                                enableBackground: 'accumulate'
                            }}
                            d="m 67.25,1002.3622 -3.46876,-3.96875 -22.00004,-25 -9.0312,7.9375 18.5,21.03125 -18.5,21.0313 9.0312,7.9375 22.00004,-25 3.46876,-3.9688 z"
                            fill="#000000" fillOpacity="1" stroke="none" marker="none" visibility="visible"
                            display="inline" overflow="visible"/>
                    </g>
                </svg>

                {this.renderHover()}
            </div>
        )
    }
}

export default TrustBox;