import React, {useState} from 'react';
import * as styles from '../../modules/Filters.module.scss';

const FilterCheckButton = ({element, onChange, loadingFilters, filters, onclickGAEvent}) => {
    const [checked, setChecked] = useState(element.checked);
    let nextDay = {};
    if (filters) {
        filters.map(function (elem) {
            if (elem.name === 'nextDay') {
                nextDay = {
                    checked: elem.data.checked,
                    disabled: elem.data.disabled
                }
            }
        });
    }
    return (
        <div className={styles.dropdownItem}>
            <div
                className={styles.filterCheckbox}
            >
                <input
                    className={styles.filterCheckboxInput}
                    type="checkbox"
                    value={element.value}
                    id={element.id}
                    checked={nextDay.checked}
                    disabled={nextDay.disabled}
                    onClick={() => {
                        element.checked = !checked;
                        onChange(element);
                        setChecked(!checked);
                        loadingFilters(true);
                        if (!nextDay.disabled) {
                            let val = !nextDay.checked;
                            onclickGAEvent({
                                action: 'FP - Top Level',
                                label: (val)? 'Express products': 'All Products',
                            })
                        }
                    }
                    }
                />
                <label
                    className={styles.filterCheckboxLabel}
                    htmlFor={element.id}
                >
                    {element.label}

                    {
                        element.count &&
                        ` (${element.count})`
                    }
                    {element.image && (
                        <div className={styles.colourSquare}>
                            <img src={element.image}/>
                        </div>
                    )}
                </label>
            </div>
        </div>
    );
};

export default FilterCheckButton;
