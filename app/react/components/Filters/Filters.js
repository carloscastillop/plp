import React from 'react';
import FilterContainer from "../FilterContainer/FilterContainer";
import FilterCheckButtonNextDay from '../FilterCheckButtonNextDay/FilterCheckButtonNextDay';
import FilterNextDayProducts from '../FilterNextDayProducts/FilterNextDayProducts';

import container from '../../modules/Container.module.scss';
import grid from '../../modules/Grid.Module.scss';
import card from "../../modules/Cards.module.scss";
import spacing from '../../modules/Spacing.module.scss';
import * as styles from '../../modules/Filters.module.scss';

class Filters extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentOpenFilter: null
        };
    }

    closeFilterContainer = () => {
        this.setState({currentOpenFilter: null});
    }

    createFilterList = () => {
        const {filters, onChange, loadingFilters, onclickGAEvent} = this.props;
        const {currentOpenFilter} = this.state;

        return filters.map((filter, index) => {
            if (filter.data !== null && filter.type === 'filterList') {
                let shouldBeDisplayed = this.shouldBeDisplayed(filter);
                if(!shouldBeDisplayed) return;
                return (
                    <div className={
                        grid.col12 + ' ' +
                        grid.colSm6 + ' ' +
                        grid.colMd4 + ' ' +
                        grid.colLg2 + ' ' +
                        grid.colXl2
                    }
                         key={'filter-' + filter.name}
                    >
                        <div
                            onClick={() => {
                                if (currentOpenFilter === filter.label) {
                                    this.setState({currentOpenFilter: null});
                                } else {
                                    this.setState({currentOpenFilter: filter.label});
                                }
                            }}>
                            <FilterContainer
                                key={filter.name}
                                filterName={filter.label}
                                filterOptions={filter.data}
                                open={currentOpenFilter === filter.label}
                                onChange={onChange}
                                closeFilterContainer={this.closeFilterContainer}
                                loadingFilters={loadingFilters}
                                onclickGAEvent={onclickGAEvent}
                            />
                        </div>
                    </div>
                )
            } else {
                return null;
            }
        });
    }

    shouldBeDisplayed = (filter) => {
        let count = 0;
        filter.data.map((element, index) => {
            if (element.value !== '' && !element.disabled) {
                count++;
            }
        });
        if (count > 0) {
            return true;
        }
        return false;
    }

    render() {
        const {
            selectedFilters,
            onChange,
            onClearAllFilters,
            isLoading,
            loadingFilters,
            filters,
            onclickGAEvent
        } = this.props;
        const hasSelectedFilters = selectedFilters.length > 0;
        return (
            <React.Fragment>
                <div className={container.container}>
                    <div className={card.card + ' ' + card.cardNoBorder}>
                        <div className={spacing.mb3}>
                            {
                                isLoading &&
                                <div className={styles.loadingFilters}></div>
                            }
                            <form>
                                <div className={grid.flexGrid}>
                                    {this.createFilterList()}
                                </div>
                                <div className={styles.selectedFiltersContainer}>
                                    <div className={styles.selectedFiltersInput}>
                                        <FilterCheckButtonNextDay
                                            key='NextDayProducts'
                                            element={FilterNextDayProducts}
                                            onChange={onChange}
                                            loadingFilters={loadingFilters}
                                            filters={filters}
                                            onclickGAEvent={onclickGAEvent}
                                        />
                                    </div>
                                    <div className={styles.selectedFiltersList}>
                                        {hasSelectedFilters &&
                                        <div className={styles.selectedFilters}>
                                            <div className={styles.selectedFiltersLeft}>
                                                <span>Selected Filters: </span>
                                                <button
                                                    className={styles.clearAll}
                                                    onClick={e => {
                                                        e.preventDefault();
                                                        onClearAllFilters();
                                                        loadingFilters(true);
                                                        onclickGAEvent({
                                                            action: 'Text Link',
                                                            label: 'Click here to reset all filters',
                                                        })
                                                    }}
                                                >
                                                    Clear All
                                                </button>
                                            </div>
                                            <div className={styles.selectedFiltersRight}>
                                                {
                                                    selectedFilters.map(filter =>
                                                        <span
                                                            className={styles.selectedFilter}
                                                            key={'selected-filter-' + filter.id.toString()}
                                                        >
                                                                    {filter.label}
                                                            <a
                                                                href="#"
                                                                className={styles.closeIcon}
                                                                onClick={e => {
                                                                    e.preventDefault();
                                                                    onChange({id: filter.id, checked: false});
                                                                    loadingFilters(true);
                                                                }}
                                                            />
                                                                </span>
                                                    )
                                                }
                                            </div>
                                        </div>
                                        }
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Filters;