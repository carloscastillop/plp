import React from 'react';
import card from '../../modules/Cards.module.scss';
import spacing from '../../modules/Spacing.module.scss';
import forms from '../../modules/Forms.module.scss';
import buttons from '../../modules/Buttons.module.scss';
import badge from '../../modules/Badge.module.scss';


const ShareLink = (shareLink) => {
    const {link} = shareLink;
    let copied = false;
    let msgStyle = {
        visibility: 'hidden'
    };

    const copyText = () => {
        /* Get the text field */
        let copyText = document.getElementById("shareLink");
        let copiedMsg = document.getElementById('copiedMsg');

        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /*For mobile devices*/

        /* Copy the text inside the text field */
        document.execCommand("copy");

        /* show message copied text */
        copiedMsg.style.visibility = 'visible';
        setTimeout(function () {
            copiedMsg.style.visibility = 'hidden';
        }, 3000);
    }

    return (
        <div className={card.card + ' ' + spacing.my4 + ' ' + card.cardGrey}>
            <div className={card.cardBody}>
                <label htmlFor="shareLink" className={spacing.mb2}>Share link</label>
                <div className={forms.inputGroup + ' ' + spacing.mb2}>
                    <input value={link} id="shareLink" type="text" className={forms.formControl} readOnly
                           placeholder="PLP share link"
                           aria-label="PLP share link" aria-describedby="shareLink"/>
                    <div className={forms.inputGroupAppend}>
                        <button
                            onClick={copyText}
                            className={buttons.btn + ' ' + buttons.btnSecondary + ' ' + buttons.btnGroupAppend}
                            type="button"
                            id="shareLink"
                        >
                            Copy link {copied}
                        </button>
                    </div>
                </div>
                <span
                    id='copiedMsg'
                    className={badge.badge + ' ' + badge.badgeSuccess}
                    style={msgStyle}
                >Link copied!!</span>
            </div>
        </div>
    );
};

export default ShareLink;