import React, { Component } from 'react';
import grid from '../../modules/Grid.Module.scss';
import utilities from '../../modules/Utilities.modules.scss';
import spacing from '../../modules/Spacing.module.scss';
import defaults from "../../configuration/defaults";
import hover from '../../modules/Hover/Hover.module.scss';

class BannerBlock extends Component {

    havePreezieKey(){
        const { preezie } = this.props;
        return preezie && preezie.key;
    }

    getLink(){
        const { preezie } = this.props;
        const { productionUrls, apiUrl } = defaults;

        return `${productionUrls ? '' : apiUrl }/pages/help-me-choose/${this.havePreezieKey() ? `?guide=${preezie.key}` : ''}`;
    }

    renderBanner(){
        const { productionUrls, apiUrl } = defaults;
        const { preezie } = this.props;

        const image = preezie.banner || '/images/res/find-my-product-banner_960_144.jpg';
        const imageAlt = 'Find my product banner';

        return (
            <img
                className={utilities.imgFluid + ' ' + hover.hvrGrow}
                style={{ cursor: 'pointer' }}
                src={productionUrls ? image : `${apiUrl}${image}`}
                alt={imageAlt}
            />
        )
    }

    render(){
        const linkTitle = 'Find your perfect match';

        return (
            <div className={grid.col12}>
                <div className={spacing.my4}>
                    <a
                        href={this.getLink()}
                        title={linkTitle} >
                        { this.renderBanner() }
                    </a>
                </div>
            </div>
        );
    }
}

export default BannerBlock;