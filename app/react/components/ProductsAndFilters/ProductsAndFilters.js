import React from 'react';
import Filters from '../Filters/Filters';
import fetchFilters from '../../services/filtersService';
import Products from '../Products/Products'

import style from "../../modules/ProductsAndFilters.module.scss";
import container from '../../modules/Container.module.scss';
import defaults from '../../configuration/defaults';

import ReactGA from 'react-ga';

ReactGA.initialize(defaults.GAtrackingId);
let sCount = 0;

class ProductsAndFilters extends React.Component {
    constructor(props) {
        super(props);
        const {categoryId, keywords, baseApiUrl, shareLink} = this.props;
        this.state = {
            filters: [],
            selectedFilters: [{categoryId: categoryId, keywords: keywords}],
            enableInfiniteScroll: false,
            isLoading: false,
            baseApiUrl: baseApiUrl,
            shareLinkCr: (sCount > 0) ? null : shareLink,
            count: sCount,
        };
    }

    componentDidMount() {
        const {selectedFilters, shareLinkCr} = this.state;
        let {baseApiUrl} = this.state;
        let updatedFilters = selectedFilters;

        fetchFilters(selectedFilters, baseApiUrl, shareLinkCr).then(filters => {
            for (const filter of filters) {
                if (typeof (filter.data) !== "undefined" && filter.data !== null && Array.isArray(filter.data)) {
                    for (const f of filter.data) {
                        if (typeof (f.checked) !== "undefined" &&
                            typeof (f.value) !== "undefined" &&
                            f.checked === true &&
                            f.value !== '') {
                            updatedFilters.push({
                                id: f.name,
                                label: f.label,
                                type: f.type,
                                value: f.value,
                                name: f.name
                            });
                        }
                    }
                }
            }
            this.setState({
                filters: filters,
                selectedFilters: updatedFilters
            });
            let newURL = location.href.split("?")[0];
            window.history.pushState('object', document.title, newURL);
        });
    }

    onFilterChange = filter => {
        const {selectedFilters, baseApiUrl} = this.state;
        let updatedFilters = [...selectedFilters];

        if (filter.checked) {
            if (filter.type === 'checkbox') {
                updatedFilters.push({
                    id: filter.id,
                    label: filter.label,
                    type: filter.type,
                    value: filter.value,
                    name: filter.name
                });
            } else {
                /***
                 * IS RADIOBUTTON
                 * TODO: improve the way the radio buttons and checkboxes name/id
                 * id: filter.name  instead  id: filter.id
                 */
                updatedFilters = selectedFilters.filter(f => f.id !== filter.name);
                if (filter.value) {
                    updatedFilters.push({
                        id: filter.name,
                        label: filter.label,
                        type: filter.type,
                        value: filter.value,
                        name: filter.name
                    });
                }
            }
        } else {
            updatedFilters = selectedFilters.filter(f => f.id !== filter.id);
        }

        fetchFilters(updatedFilters, baseApiUrl).then(filters => {
            this.setState(
                {
                    selectedFilters: updatedFilters,
                    filters: filters,
                    isLoading: false,
                    count: sCount,
                });
        });
    };

    onClearAllFilters = () => {
        const {selectedFilters, baseApiUrl} = this.state;
        const updatedFilters = selectedFilters.filter(f => f.hasOwnProperty('categoryId'));
        this.setState({selectedFilters: updatedFilters});

        fetchFilters(updatedFilters, baseApiUrl).then(filters => {
            this.setState(
                {
                    filters: filters,
                    isLoading: false,
                    count: sCount,
                }
            );
        });
    }

    loadingFilters = (state = true) => {
        this.setState({isLoading: state});
    }

    onclickGAEvent = (args = {}) => {
        if (defaults.isDebug) {
            return console.log(args);
        }
        let categoryName = (args.category) ? args.category : 'LP Interaction';
        let actionName = (args.action) ? args.action : 'Image Link';
        let labelName = (args.label) ? args.label : 'Some product!';
        ReactGA.event({
            category: categoryName,
            action: actionName,
            label: labelName
        });
    }

    render() {
        const {filters, selectedFilters, enableInfiniteScroll, isLoading, baseApiUrl, shareLinkCr} = this.state;

        return (
            <React.Fragment>
                <div className={style.productsAndFiltersContainer}>
                    <Filters
                        filters={filters}
                        onChange={this.onFilterChange}
                        onClearAllFilters={this.onClearAllFilters}
                        selectedFilters={selectedFilters.filter(filter => !filter.hasOwnProperty('categoryId') && !(filter.id === 'es'))}
                        loadingFilters={this.loadingFilters}
                        isLoading={isLoading}
                        onclickGAEvent={this.onclickGAEvent}
                    />
                    <div className={container.container}>
                        <Products
                            selectedFilters={selectedFilters}
                            enableInfiniteScroll={enableInfiniteScroll}
                            onclickGAEvent={this.onclickGAEvent}
                            baseApiUrl={baseApiUrl}
                            shareLink={shareLinkCr}
                        />
                    </div>
                </div>
            </React.Fragment>
        )
    }
};

export default ProductsAndFilters;
