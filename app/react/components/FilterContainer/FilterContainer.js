import React from 'react';
import FilterContainerStyle from '../../modules/FilterContainer.module.scss'
import FilterRadioButton from '../FilterRadioButton/FilterRadioButton';
import FilterCheckButton from '../FilterCheckButton/FilterCheckButton';

const FilterContainer = (props) => {
    const {filterName, filterOptions, onChange, open, closeFilterContainer, loadingFilters, onclickGAEvent} = props;
    const filterContainerRef = React.createRef();

    const handleClickOutside = event => {
        if (filterContainerRef.current && !filterContainerRef.current.contains(event.target)) {
            document.removeEventListener('mousedown', handleClickOutside, false);
            closeFilterContainer();
        }
    };

    const selectedTagFlag = filterOptions => {
        let count = 0;
        filterOptions.map((element, index) => {
            if (element.value !== '' && element.checked) {
                count++;
            }
        })
        return parseInt(count);
    }

    const checkSelected = selectedTagFlag(filterOptions);

    document.addEventListener('mousedown', handleClickOutside, false);

    return (
        <React.Fragment>
            <div
                className={
                    FilterContainerStyle.dropdown + ' ' +
                    (checkSelected > 0 ? FilterContainerStyle.ContainSelectedFilters : '')
                }
                attr-selected={checkSelected}
            >
                <button
                    type="button"
                    className={FilterContainerStyle.btn + ' ' + FilterContainerStyle.filterToggler}
                    disabled={filterOptions.length === 0}
                    data-name={filterName}
                >
                    {filterName}
                </button>
                {open && (
                    <div className={FilterContainerStyle.dropdownMenu + ' ' + FilterContainerStyle.show}
                         ref={filterContainerRef}>
                        {filterOptions.map((value, index) => {
                            if (value.type == 'checkbox') {
                                return <FilterCheckButton
                                    key={value.id}
                                    element={value}
                                    onChange={onChange}
                                    loadingFilters={loadingFilters}
                                    onclickGAEvent={onclickGAEvent}
                                    gaActionName={filterName}
                                />;
                            }
                            return <FilterRadioButton
                                key={value.id}
                                element={value}
                                onChange={onChange}
                                loadingFilters={loadingFilters}
                                onclickGAEvent={onclickGAEvent}
                                gaActionName={filterName}
                            />;
                        })}
                    </div>
                )}
            </div>
        </React.Fragment>
    )
};

export default FilterContainer;