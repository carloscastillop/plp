import React, {Component} from 'react';
import QuickView from '../ProductQuickView/ProductQuickView';
import TrustBox from "../Trustbox/Trustbox";

import {ImageLoader} from "@quayside/react-elements";

import style from "../../modules/ProductBlock.module.scss";
import grid from '../../modules/Grid.Module.scss';
import card from '../../modules/Cards.module.scss';
import spacing from "../../modules/Spacing.module.scss";
import fadein from '../../modules/FadeIn.module.scss';

import defaults from '../../configuration/defaults';

class ProductBlock extends Component {

    constructor(props) {
        super(props);

        this.state = {
            hover: false
        };

        this.productRef = React.createRef();
        this.timer = 0;
    }

    hover = () => {
        this.timer = setTimeout(this.hoverDelayed, 100);
    }

    hoverDelayed = () => {

        if (this.timer) {
            clearTimeout(this.timer)
        }
        document.addEventListener('mousemove', this.handleMouseOutside);

        this.setState({
            hover: true
        })
    }

    handleMouseOutside = (event) => {

        if (
            this.productRef.current && !this.productRef.current.contains(event.target)
        ) {
            if (this.timer) {
                clearTimeout(this.timer)
            }
            document.removeEventListener('mousemove', this.handleMouseOutside);

            this.setState({
                hover: false
            })
        }
    };

    convertHTMLEntity = (text) => {
        const span = document.createElement('span');

        return text
            .replace(/&[#A-Za-z0-9]+;/gi, (entity, position, text) => {
                span.innerHTML = entity;
                return span.innerText;
            });
    }

    capitalizeFLetter = (string = '') => {
        return string[0].toUpperCase() + string.slice(1);
    }

    render() {
        const {item, review, side, onclickGAEvent} = this.props;
        const {hover} = this.state;
        const {productionUrls} = defaults;

        let numReviews = 'No reviews';
        if (review && review.numberOfReviews) {
            numReviews = `${review.numberOfReviews.total} review${review.numberOfReviews.total === 1 ? '' : 's'}`;
        }
        return (
            <div
                className={grid.col12 + ' ' +
                grid.colSm6 + ' ' +
                grid.colMd4 + ' ' +
                grid.colLg3 + ' ' +
                spacing.mb4}
            >
                <div
                    data-product-id={item.id}
                    className={
                        card.card + ' ' +
                        card.cardFullHeight + ' ' +
                        style.productDisplay + ' ' +
                        card.cardLight}
                >
                    <QuickView side={side} item={item}/>

                    {
                        item.featured &&
                        <div className={style.featured}>
                            {item.featured}
                        </div>
                    }
                    <div
                        className={card.cardBody}
                        ref={this.productRef}
                        onMouseEnter={() => this.hover(true)}
                    >
                        <div
                            className={style.imageBlock}
                        >
                            <a
                                href={item.link}
                                className={fadein.fadeIn}
                                onClick={() => { onclickGAEvent({
                                    action: 'Image Link',
                                    label: item.name,
                                })}}
                            >
                                <ImageLoader
                                    src={productionUrls ? item.preview : `${defaults.apiUrl}${item.preview}`}
                                    alt={item.name}
                                />
                                {item.nextDay &&
                                <div className={style.nextDay}>
                                    <div className={style.nextDayIcon}></div>
                                </div>
                                }
                            </a>
                        </div>


                        <div className={style.brandLogo}>
                            {
                                (item.brand.logo) ?
                                    (<img
                                        src={productionUrls ? item.brand.logo : `${defaults.apiUrl}${item.brand.logo}`}
                                        alt={item.brand.name}
                                        title={item.brand.name}
                                    />) :
                                    (
                                        <h5
                                            className={spacing.my1}
                                        >
                                            {item.brand.name}
                                        </h5>
                                    )
                            }
                        </div>

                        <div className={style.tags}>
                            {
                                item.tag &&
                                <div
                                    className={style.tag}>
                                    {item.tag}
                                </div>
                            }
                            {
                                item.express &&
                                <div
                                    className={style.tag}
                                    style={{
                                        background: '#215A90'
                                    }}>
                                    24h
                                </div>
                            }
                        </div>
                        <div className={style.textLink}>
                            <h2>
                                <a
                                    href={item.link}
                                    onClick={() => { onclickGAEvent({
                                        action: 'Text Link',
                                        label: item.name,
                                    })}}
                                >
                                    {item.name}
                                </a>
                            </h2>

                        </div>
                        <div className={style.prices}>
                            {item.price.length > 0 &&
                            item.price.map((item, index) =>
                                <div
                                    className={style.priceBlock}
                                    key={'price-' + item.label + '-' + item.id + '-' + item.index}
                                >
                                    {this.capitalizeFLetter(item.label)} from: <span
                                    className={style.priceBlockPrice}>{this.convertHTMLEntity(item.price)}</span>
                                </div>
                            )}
                        </div>
                        {
                            review && <TrustBox
                                review={review}
                                numReviews={numReviews}
                            />
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default ProductBlock;