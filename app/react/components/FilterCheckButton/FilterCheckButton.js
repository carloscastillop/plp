import React, {useState} from 'react';
import * as styles from '../../modules/Filters.module.scss';

const FilterCheckButton = ({element, onChange, loadingFilters, onclickGAEvent, gaActionName}) => {
    const [checked, setChecked] = useState(element.checked);

    return (
        <div className={styles.dropdownItem}>
            <div
                className={styles.filterCheckbox}
                onClick={() => {
                    element.checked = !checked;
                    onChange(element);
                    setChecked(!checked);
                    loadingFilters(true);
                    onclickGAEvent({
                        action: 'FP - By ' + gaActionName,
                        label: element.label,
                    });
                }
            }>
            <input
                className={styles.filterCheckboxInput}
                type="checkbox"
                value={element.value}
                id={element.id}
                checked={checked}
            />
            <label
                className={styles.filterCheckboxLabel}
                htmlFor={element.id}
            >
                {element.label}

                {
                    element.count &&
                    ` (${element.count})`
                }
                {element.image && (
                    <div className={styles.colourSquare}>
                        <img src={element.image}/>
                    </div>
                )}
            </label>
            </div>
        </div>
    );
};

export default FilterCheckButton;
