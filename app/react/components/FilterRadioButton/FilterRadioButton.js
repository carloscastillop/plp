import React, { useState } from 'react';
import * as styles from '../../modules/Filters.module.scss';

const FilterRadioButton = ({ element, onChange, loadingFilters, onclickGAEvent, gaActionName }) => {
    const [checked, setChecked] = useState(element.checked);

    const isDisabled = element.disabled;

    return (
        <div className={styles.dropdownItem}>
            <div
                className={styles.filterCheckbox}
                onClick={() => {
                    if(isDisabled) return;
                    element.checked = !checked;
                    onChange(element);
                    setChecked(!checked);
                    loadingFilters(true);
                    onclickGAEvent({
                        action: 'FP - By '+gaActionName,
                        label: element.label,
                    });
                }
            }>
                <input
                    className={styles.filterCheckboxInput}
                    type="radio"
                    name={element.name}
                    id={element.id}
                    value={element.value}
                    checked={checked}
                    disabled={isDisabled}
                />
                <label
                    className={styles.filterCheckboxLabel}
                    htmlFor={element.id}
                >
                    {element.label}
                </label>
            </div>
        </div>
    );
};

export default FilterRadioButton;