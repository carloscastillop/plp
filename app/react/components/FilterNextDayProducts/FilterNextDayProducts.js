const FilterNextDayProducts = {
    checked: false,
    disabled: false,
    id: "es",
    label: "Next Day products",
    name: "es",
    type: "checkbox",
    value: 1
}
export default FilterNextDayProducts;