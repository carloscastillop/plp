import React from 'react';

import defaults from '../../configuration/defaults';
import style from '../../modules/ProductQuickView.module.scss';
import utilities from '../../modules/Utilities.modules.scss';
import product from "../../modules/ProductBlock.module.scss";

export default function (props) {
    const item = props.item
    const { productionUrls } = defaults;

    return (
        <div
            className={
                style[`view${props.side}`] + ' ' + product.onHover
            }>
            <div className={style.inner}>
                <h3 className={product.onHover}>{item.title}</h3>
                {
                    (typeof(item.colours) !== "undefined" && item.colours.length > 0 ) &&
                    <div className={style.descContainer + ' ' + style.descColours}>
                        <h5>Available in {item.colours.length} colours</h5>
                        <ul>
                            {item.colours.map((value, index) => {
                                return <li key={'colours-'+'-'+item.id+'-'+index}>
                                    <img
                                        src={productionUrls ? value : `${defaults.apiUrl}${value}`}
                                        className={utilities.loadingBg}
                                    />
                                </li>
                            })}
                        </ul>
                    </div>
                }
                {
                    (typeof(item.sizes) !== "undefined" && item.sizes.length > 0 ) &&
                    <div className={style.descContainer + ' ' + style.descSizes}>
                        <h5>Available in {item.sizes.length} sizes</h5>
                        {
                            (typeof(item.sizes) !== "undefined" && item.sizes.length > 0 ) &&
                            <ul>
                                {item.sizes.map((value, index) => {
                                    return <li key={'sizes-'+'-'+item.id+'-'+index}>{value}</li>
                                })}
                            </ul>
                        }
                    </div>
                }

                <div className={style.descContainer}>
                    <h5>Next day dispatch?</h5>
                    <p>{item.dispatch}</p>
                </div>

                {
                    (typeof(item.customisations) !== "undefined" && item.customisations.length > 0 ) &&
                    <div className={style.descContainer + ' ' + style.descCustomisations}>
                        <h5>{item.customisations.length} Customisations available:</h5>
                        <ul>
                            {item.customisations.map((value, index) => {
                                return <li key={'customisations-'+'-'+item.id+'-'+index}>{value}</li>
                            })}
                        </ul>
                    </div>
                }

                <div className={style.descContainer}>
                    <h5>Stock availability</h5>
                    <p>{item.stock}</p>
                </div>
            </div>
        </div>
    )
}